package entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class ThirtyDayLoser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private LocalDate date;
    private double thirtyDayMovement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getThirtyDayMovement() {
        return thirtyDayMovement;
    }
    public void setThirtyDayMovement(double thirtyDayMovement) {
        this.thirtyDayMovement = thirtyDayMovement;
    }
}
