package entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class SevenDayLoser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private LocalDate date;
    private double sevenDayMovement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getSevenDayMovement() {
        return sevenDayMovement;
    }
    public void setSevenDayMovement(double sevenDayMovement) {
        this.sevenDayMovement = sevenDayMovement;
    }
}
