package entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class OneDayLoser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private LocalDate date;
    private double oneDayMovement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getOneDayMovement() {
        return oneDayMovement;
    }
    public void setOneDayMovement(double dailyMovement) {
        this.oneDayMovement = dailyMovement;
    }
}
