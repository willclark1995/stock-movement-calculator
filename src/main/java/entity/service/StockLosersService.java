package entity.service;

import entity.OneDayLoser;
import entity.SevenDayLoser;
import entity.ThirtyDayLoser;
import gson.StockGainLoss;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDate;

@ApplicationScoped
public class StockLosersService {

    @Inject
    EntityManager em;

    @Transactional
    public void createDailyLoser(StockGainLoss stock) {
        OneDayLoser oneDayLoser = new OneDayLoser();
        oneDayLoser.setDate(LocalDate.now());
        oneDayLoser.setName(stock.getName());
        oneDayLoser.setOneDayMovement(stock.getDailyMovement());
        em.persist(oneDayLoser);
    }

    @Transactional
    public void createWeeklyLoser(StockGainLoss stock) {
        SevenDayLoser sevenDayLoser = new SevenDayLoser();
        sevenDayLoser.setDate(LocalDate.now());
        sevenDayLoser.setName(stock.getName());
        sevenDayLoser.setSevenDayMovement(stock.getDailyMovement());
        em.persist(sevenDayLoser);
    }

    @Transactional
    public void createMonthlyLoser(StockGainLoss stock) {
        ThirtyDayLoser thirtyDayLoser = new ThirtyDayLoser();
        thirtyDayLoser.setDate(LocalDate.now());
        thirtyDayLoser.setName(stock.getName());
        thirtyDayLoser.setThirtyDayMovement(stock.getDailyMovement());
        em.persist(thirtyDayLoser);
    }
}
