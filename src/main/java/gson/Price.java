package gson;

import java.util.List;
import java.util.Optional;

public class Price {

    private List<String> c;
    private List<String> t;

    public Price(List<String> c, List<String> t) {
        this.c = c;
        this.t = t;
    }

    public Optional<List<String>> getClosePrice() {
        return Optional.ofNullable(c);
    }

    public Optional<List<String>> getTimestamp() {
        return Optional.ofNullable(t);
    }

}
