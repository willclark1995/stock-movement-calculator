package gson;

import java.util.List;

public class Constituents {

    private List<String> constituents;
    private String symbol;

    public Constituents(List<String> constituents, String symbol) {
        this.constituents = constituents;
        this.symbol = symbol;
    }

    public List<String> getConstituents() {
        return constituents;
    }

    public String getSymbol() {
        return symbol;
    }
}

