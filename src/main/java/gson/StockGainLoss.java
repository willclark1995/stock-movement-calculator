package gson;

public class StockGainLoss {

    private String name;
    private Double dailyMovement;
    private Double sevenDayMovement;
    private Double thirtyDayMovement;

    public StockGainLoss(String name, Double dailyMovement, Double sevenDayMovement, Double thirtyDayMovement) {
        this.name = name;
        this.dailyMovement = dailyMovement;
        this.sevenDayMovement = sevenDayMovement;
        this.thirtyDayMovement = thirtyDayMovement;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getDailyMovement() {
        return dailyMovement;
    }

    public void setDailyMovement(Double dailyMovement) {
        this.dailyMovement = dailyMovement;
    }

    public Double getSevenDayMovement() {
        return sevenDayMovement;
    }

    public void setSevenDayMovement(Double sevenDayMovement) {
        this.sevenDayMovement = sevenDayMovement;
    }

    public Double getThirtyDayMovement() {
        return thirtyDayMovement;
    }

    public void setThirtyDayMovement(Double thirtyDayMovement) {
        this.thirtyDayMovement = thirtyDayMovement;
    }
}
