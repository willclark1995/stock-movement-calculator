package connectivity;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import java.io.IOException;

public class HttpRequestCreator {

    private Gson gson = new Gson();

    public <T> T get(OkHttpClient client, String url, Class<T> responseClass) {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        try {
            Response response = client.newCall(request).execute();
            return responseClass.cast(gson.fromJson(response.body().string(), responseClass));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
