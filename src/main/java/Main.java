import com.squareup.okhttp.OkHttpClient;
import connectivity.HttpRequestCreator;
import data.Indices;
import entity.service.StockLosersService;
import gson.Constituents;
import gson.Price;
import gson.StockGainLoss;
import io.quarkus.scheduler.Scheduled;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@ApplicationScoped
public class Main {

    @Inject
    StockLosersService losersService;

    @ConfigProperty(name = "api.key")
    String key;

    private final OkHttpClient client = new OkHttpClient();
    private final HttpRequestCreator httpRequestCreator = new HttpRequestCreator();
    private final String BASE_URL = "https://finnhub.io/api/v1/";

    private Optional<List<Constituents>> retrieveIndexConstituents() {
        return Optional.of(Arrays.stream(Indices.values()).map(indices -> {
            String url = String.format(
                    BASE_URL + "index/constituents?symbol=^%s&token=%s",
                    indices.toString(),
                    key
            );
            return httpRequestCreator.get(client, url, Constituents.class);
        }).collect(Collectors.toList()));
    }

    private Optional<Map<String, Price>> retrievePriceData(List<Constituents> constituents) {
        return Optional.of(constituents.stream().flatMap(index -> index.getConstituents().stream().map(stock -> {
            String url = String.format(
                    BASE_URL + "stock/candle?symbol=%s&resolution=D&from=%s&to=%s&token=%s",
                    stock,
                    Instant.now().minus(31, ChronoUnit.DAYS).getEpochSecond(),
                    Instant.now().minus(1, ChronoUnit.DAYS).getEpochSecond(),
                    key
            );
            Price price = httpRequestCreator.get(client, url, Price.class);
            return Map.entry(stock, price);
        })).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
    }

    public Optional<List<StockGainLoss>> generateDailyWeeklyMonthlyMovementData(Map<String, Price> prices) {
        return Optional.of(prices.entrySet().stream().map(mapEntry -> {
            Double currentClosePrice = mapEntry.getValue().getClosePrice().map(p -> Double.valueOf(p.get(p.size() - 1))).orElse(0d);
            Double oneDayClosePrice = mapEntry.getValue().getClosePrice().map(p -> Double.valueOf(p.get(p.size() - 2))).orElse(0d);
            Double sevenDayClosePrice = mapEntry.getValue().getClosePrice().map(p -> Double.valueOf(p.get(p.size() - 6))).orElse(0d);
            Double thirtyDayClosePrice = mapEntry.getValue().getClosePrice().map(p -> Double.valueOf(p.get(0))).orElse(0d);

            Double oneDayLossGain = ((currentClosePrice - oneDayClosePrice) / oneDayClosePrice) * 100;
            Double sevenDayLossGain = ((currentClosePrice - sevenDayClosePrice) / sevenDayClosePrice) * 100;
            Double thirtyDayLossGain = ((currentClosePrice - thirtyDayClosePrice) / thirtyDayClosePrice) * 100;

            return new StockGainLoss(mapEntry.getKey(), oneDayLossGain, sevenDayLossGain, thirtyDayLossGain);
        }).collect(Collectors.toList()));
    }

    public List<StockGainLoss> generateDailyLosers (List<StockGainLoss> dailyWeeklyMonthlyMovement, int limit) {
        return dailyWeeklyMonthlyMovement.stream()
                .sorted(Comparator.comparingDouble(StockGainLoss::getDailyMovement))
                .limit(limit)
                .collect(Collectors.toList());
    }

    public List<StockGainLoss> generateWeeklyLosers (List<StockGainLoss> dailyWeeklyMonthlyMovement, int limit) {
        return dailyWeeklyMonthlyMovement.stream()
                .sorted(Comparator.comparingDouble(StockGainLoss::getSevenDayMovement))
                .limit(limit)
                .collect(Collectors.toList());
    }

    public List<StockGainLoss> generateMonthlyLosers (List<StockGainLoss> dailyWeeklyMonthlyMovement, int limit) {
        return dailyWeeklyMonthlyMovement.stream()
                .sorted(Comparator.comparingDouble(StockGainLoss::getThirtyDayMovement))
                .limit(limit)
                .collect(Collectors.toList());
    }

    @Scheduled(cron = "0 0 4 * * ?")
    public void daily() {
        retrieveIndexConstituents()
                .flatMap(this::retrievePriceData)
                .flatMap(this::generateDailyWeeklyMonthlyMovementData)
                .ifPresent(movementData -> {
            generateDailyLosers(movementData, 20).forEach(odl -> losersService.createDailyLoser(odl));
            generateWeeklyLosers(movementData, 20).forEach(sdl -> losersService.createWeeklyLoser(sdl));
            generateMonthlyLosers(movementData, 20).forEach(tdl -> losersService.createMonthlyLoser(tdl));
        });
    }
    
    public static void main(String[] args) {
        Quarkus.run();
    }
}

