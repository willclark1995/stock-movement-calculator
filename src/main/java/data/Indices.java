package data;

public enum Indices {

    GSPC("S&P 500"),
    NDX("NASDAQ 100"),
    DJI("Dow Jones Industrial Average"),
    MID("S&P Mid-Cap 400"),
    SP600("S&P SmallCap 600"),
    RUI("Russell 1000"),
    RUMIC("Russell Microcap"),
    RMCC("Russell Midcap"),
    RT200("Russell Midcap");

    private final String label;

    private Indices(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
