CREATE TABLE IF NOT EXISTS onedayloser (
  id SERIAL NOT NULL,
  name text NOT NULL,
  onedaymovement decimal(9,2),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS sevendayloser (
  id SERIAL NOT NULL,
  name text NOT NULL,
  sevendaymovement decimal(9,2),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS thirtydayloser (
  id SERIAL NOT NULL,
  name text NOT NULL,
  thirtydaymovement decimal(9,2),
  PRIMARY KEY (id)
);