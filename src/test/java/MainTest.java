import entity.OneDayLoser;
import entity.SevenDayLoser;
import entity.ThirtyDayLoser;
import gson.Price;
import gson.StockGainLoss;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class MainTest {

    @Inject
    Main m;

    String stockName = "STOCK";

    Price priceData = new Price(List.of("30", "29", "28", "27", "26", "25", "24", "23", "22", "21",
            "20", "19", "18", "17", "16", "15", "14", "13", "21", "11",
            "10", "9", "8", "7", "6", "5", "4", "3", "2", "1"),
            List.of("30", "29", "28", "27", "26", "25", "24", "23", "22", "21",
                    "20", "19", "18", "17", "16", "15", "14", "13", "21", "11",
                    "10", "9", "8", "7", "6", "5", "4", "3", "2", "1"));

    List<StockGainLoss> losers = List.of(
            new StockGainLoss("Stock_1", -5d, -10d, -20d),
            new StockGainLoss("Stock_2", -6d, -10d, -19d),
            new StockGainLoss("Stock_3", -7d, -10d, -18d),
            new StockGainLoss("Stock_4", -8d, -11d, -17d),
            new StockGainLoss("Stock_5", -9d, -12d, -16d),
            new StockGainLoss("Stock_6", -10d, -13d, -15d),
            new StockGainLoss("Stock_7", -11d, -14d, -14d),
            new StockGainLoss("Stock_8", -12d, -15d, -13d),
            new StockGainLoss("Stock_9", -13d, -10d, -12d),
            new StockGainLoss("Stock_10", -14d, -10d, -11d)
            );

    @Test
    @DisplayName("The application can correctly calculate the one, seven and thirty data movement for a stock")
    public void generateWeeklyMonthlyMovementDataTest () {

        Optional<List<StockGainLoss>> movementData = m.generateDailyWeeklyMonthlyMovementData(Collections.singletonMap(stockName, priceData));
        Double expectedThirtyDayMovement = -96.66666666666667;
        Double expectedSevenDayMovement = -83.33333333333334;
        Double expectedOneDayMovement = -50.0;

        assertEquals(movementData.get().get(0).getName(), stockName);
        assertEquals(movementData.get().get(0).getThirtyDayMovement(), expectedThirtyDayMovement);
        assertEquals(movementData.get().get(0).getSevenDayMovement(), expectedSevenDayMovement);
        assertEquals(movementData.get().get(0).getDailyMovement(), expectedOneDayMovement);
    }

    @Test
    @DisplayName("The application can provide a list of the stocks with the largest daily decline")
    public void generateDailyLosersTest () {

        List<StockGainLoss> expectedDailyLosers = List.of(
                new StockGainLoss("Stock_10", -14d, -10d, -11d),
                new StockGainLoss("Stock_9", -13d, -10d, -12d),
                new StockGainLoss("Stock_8", -12d, -15d, -13d)
        );

        List<StockGainLoss> dailyLosers = m.generateDailyLosers(losers, 3);

        assertEquals(dailyLosers.get(0).getName(), expectedDailyLosers.get(0).getName());
        assertEquals(dailyLosers.get(1).getName(), expectedDailyLosers.get(1).getName());
        assertEquals(dailyLosers.get(2).getName(), expectedDailyLosers.get(2).getName());
    }

    @Test
    @DisplayName("The application can provide a list of the stocks with the largest weekly decline")
    public void generateWeeklyLosersTest () {
        List<StockGainLoss> expectedWeeklyLosers = List.of(
                new StockGainLoss("Stock_8", -12d, -15d, -13d),
                new StockGainLoss("Stock_7", -11d, -14d, -14d),
                new StockGainLoss("Stock_6", -10d, -13d, -15d)
        );

        List<StockGainLoss> weeklyLosers = m.generateWeeklyLosers(losers, 3);

        assertEquals(weeklyLosers.get(0).getName(), expectedWeeklyLosers.get(0).getName());
        assertEquals(weeklyLosers.get(1).getName(), expectedWeeklyLosers.get(1).getName());
        assertEquals(weeklyLosers.get(2).getName(), expectedWeeklyLosers.get(2).getName());
    }

    @Test
    @DisplayName("The application can provide a list of the stocks with the largest monthly decline")
    public void generateMonthlyLosersTest () {
        List<StockGainLoss> expectedMonthlyLosers = List.of(
                new StockGainLoss("Stock_1", -5d, -10d, -20d),
                new StockGainLoss("Stock_2", -6d, -10d, -19d),
                new StockGainLoss("Stock_3", -7d, -10d, -18d)
        );

        List<StockGainLoss> monthlyLosers = m.generateMonthlyLosers(losers, 3);

        assertEquals(monthlyLosers.get(0).getName(), expectedMonthlyLosers.get(0).getName());
        assertEquals(monthlyLosers.get(1).getName(), expectedMonthlyLosers.get(1).getName());
        assertEquals(monthlyLosers.get(2).getName(), expectedMonthlyLosers.get(2).getName());
    }
}
