# Stock Movement Calculator README #

This README will explain how to set up and run the Stock Movement Calculator on your machine.

### What is the aim of this project? ###

The code enclosed in this project prototypes a Quarkus Hibernate application running alongside a Postgres database.

Everyday at 4am, the application will fetch stock information from Finnhub.io about all constituents from a pre-selected list of indices. It will then identify which stocks have had the largest daily, weekly and monthly decrease in value, in terms of % value. It then records these results to a locally running Postgres database using Hibernate.

### Prerequisites ###

* JDK 11+ installed with JAVA_HOME configured appropriately
* Apache Maven 3.8.1+
* Docker Desktop (preferably the latest version)
* A command shell/ bash terminal
* An API_Key from Finnhub.io.

### Running the application ###

__First time run only:__ 

* Replace the api.key value in the application.properties file with your own API key.
* In the command line, package the stock-movement-calculator project using `mvnw package`.
* Then, create the docker image using the command: `docker build -f src/main/docker/Dockerfile.jvm -t quarkus/stock-movement-calculator-jvm .`.

__Thereafter:__

* A docker-compose.yaml exists in the main project directory which runs the two image detailed above and the Postgres image.
